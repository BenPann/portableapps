Since SmartGit 18.2, only 64-bit Windows systems are officially
supported.

If you are running a Windows 32-bit Windows system, you may try
to get SmartGit working according to following instructions:

https://www.syntevo.com/blog/?p=4558

You can get smartgit32.exe from the SmartGit 18.1 portable
archive:

<https://www.syntevo.com/downloads/smartgit/smartgit-portable-18_1_5.7z>
