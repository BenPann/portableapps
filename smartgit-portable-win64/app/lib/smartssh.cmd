@echo off
"%SMARTGIT_JAVA_HOME%\bin\java.exe" "-XX:ErrorFile=%JAVA_ERROR_FILE%" -Xms1m -XX:InitialCodeCacheSize=160K -XX:MarkStackSize=32K -XX:+UseParallelGC -XX:ParallelGCThreads=1 -Xmx64m -Djava.net.preferIPv4Stack=true -cp "%SMARTGIT_CLASSPATH%" -Dsmartgit.logging=true com.syntevo.dvcs.transport.ssh.SdSshMain %*
exit 0
