#!/bin/bash
"${SMARTGIT_JAVA_HOME}/bin/java" -Xms1m -XX:InitialCodeCacheSize=160K -XX:MarkStackSize=32K -XX:+UseParallelGC -XX:ParallelGCThreads=1 -Xmx64m -cp "${SMARTGIT_CLASSPATH}" -Dsmartgit.logging=false -Djava.net.preferIPv4Stack=true com.syntevo.smartgit.SgRebaseInteractiveMain "$@"
exit $?