// solution: add polyfill and using function instead of arrow function for IE11.
if (typeof Promise !== "function") {
  document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.min.js"><\/script>');
}
var clientData = {
  txnId: "txn-id",
  appId: "app-id",
  appKey: "app-key",
  environment: "TEST"
};
requestDdcaResult(clientData, timeout).then(
  function (ddcaResult) {
    console.log(ddcaResult)
  }).catch(function (error) {
    console.log("error catched")
  });